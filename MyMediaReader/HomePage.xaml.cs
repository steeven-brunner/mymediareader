﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace MyMediaReader
{
    /// <summary>
    /// Logique d'interaction pour HomePage.xaml
    /// </summary>
    public partial class HomePage : Window
    {
        public HomePage()
        {
            InitializeComponent();
        }

        private void LoadWithLanguage(object sender, MouseButtonEventArgs e)
        {
            Border border = sender as Border;

            MainWindow mainWindow = new MainWindow(border.Tag.ToString());
            mainWindow.Show();
            this.Close();

            /* private void SetLanguageDictionary()
         {
             ResourceDictionary dict = new ResourceDictionary();
             switch (Thread.CurrentThread.CurrentCulture.ToString())
             {
                 case "en-US":
                     dict.Source = new Uri("..\\Resources\\StringResources.xaml", UriKind.Relative);
                     break;
                 case "fr-CA":
                     dict.Source = new Uri("..\\Resources\\StringResources.fr-CA.xaml", UriKind.Relative);
                     break;
                 default:
                     dict.Source = new Uri("..\\Resources\\StringResources.xaml", UriKind.Relative);
                     break;
             }
             this.Resources.MergedDictionaries.Add(dict);
         }*/
        }
    }
}
